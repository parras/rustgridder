use ndarray::prelude::*;
use ndarray::Zip;
use ndarray_rand::rand_distr::Uniform;
use ndarray_rand::RandomExt;
use num::complex::Complex;
use rand::Rng;
use std::f64::consts::PI;

const SPEEDOFLIGHT: f64 = 299792458.;

fn explicit_gridder(
    uvw: &Array2<f64>,
    freq: &Array1<f64>,
    vis: &Array2<Complex<f64>>,
    nxdirty: usize,
    nydirty: usize,
    pixsizex: f64,
    pixsizey: f64,
    apply_w: bool,
) -> Array2<f64> {
    let nxh = nxdirty as f64 / 2.;
    let xs = (Array::linspace(-1. * nxh, nxh - 1., nxdirty) * pixsizex).insert_axis(Axis(1));
    let nyh = nydirty as f64 / 2.;
    let ys = Array::linspace(-1. * nyh, nyh - 1., nydirty) * pixsizey;
    let xim = xs.broadcast((nxdirty, nydirty)).unwrap();
    let yim = ys.broadcast((nxdirty, nydirty)).unwrap();
    let eps = xim.mapv(|a| a.powi(2)) + yim.mapv(|a| a.powi(2));
    let mut dirty = Array2::zeros((nxdirty, nydirty));
    let nm1 = if apply_w {
        eps.mapv(|a| -1. * a / ((f64::sqrt(1. - a)) + 1.))
    } else {
        Array2::<f64>::zeros((nxdirty, nydirty))
    };
    for (row, uvwiter) in vis.outer_iter().zip(uvw.outer_iter()) {
        for (myvis, ff) in row.iter().zip(freq.iter()) {
            Zip::from(&mut dirty)
                .and(&xim)
                .and(&yim)
                .and(&nm1)
                .apply(|w, &ll, &mm, &nn| {
                    let e0 = 2. * PI * ff / SPEEDOFLIGHT;
                    let e1 = uvwiter[0] * ll + uvwiter[1] * mm - uvwiter[2] * nn;
                    *w += (myvis * (Complex::<f64>::i() * e0 * e1).exp()).re
                });
        }
    }
    dirty / (nm1 + 1.)
}

fn main() {
    let nrow = 1000;
    let fov = 20.;
    let nxdirty = 16;
    let nydirty = 64;
    let nchan = 2;
    let f0 = 1e9;
    let freq = Array::linspace(f0, 2. * f0, nchan);
    let pixsizex = fov * PI / 180. / nxdirty as f64;
    let pixsizey = fov * PI * 1.1 / 180. / nydirty as f64;
    let uvw = Array::random((nrow, 3), Uniform::new(-0.5, 0.5));
    let apply_w = true;
    let mut rng = rand::thread_rng();
    let mut vec = Vec::with_capacity(nrow * nchan);
    for _ in 0..nrow * nchan {
        vec.push(Complex::<f64>::new(
            rng.gen_range(-0.5, 0.5),
            rng.gen_range(-0.5, 0.5),
        ));
    }
    let vis = Array2::from_shape_vec((nrow, nchan), vec).unwrap();
    let dirty = explicit_gridder(
        &uvw, &freq, &vis, nxdirty, nydirty, pixsizex, pixsizey, apply_w,
    );
    println!("{}", dirty);
}
